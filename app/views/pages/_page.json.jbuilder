json.extract! page, :id, :slug, :menu_label, :published_at, :published, :priority, :title, :description, :h1, :content, :created_at, :updated_at
json.url page_url(page, format: :json)