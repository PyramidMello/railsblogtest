class Page < ApplicationRecord
before_create :create_slug
  validates :slug, uniqueness: true, presence: true, length: {maximum:30}  

  def to_param
    "#{slug}".downcase 
  end

  def create_slug
    self.slug=self.slug.parameterize
  end
                               
end
