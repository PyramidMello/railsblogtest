class PagesController < ApplicationController

  def index
    @check = false
    @titletext = "Test Blog, Test Title. CRUD With Addons."
    @metadescription = "Page contained a list of created page."
     @pages = Page.all.order("priority")
     @pagesN = Page.where(params[:page]).order("priority")
     @pagesN.each do |page|
      if page.published == false
        @check = true
        end
     end 
  end

  def new
    @titletext = "Create new page - Test Blog"
    @metadescription = "Create new page with many parameters"
    @pages = Page.all
    @page = Page.new
  end

  def edit
    @page = Page.where(:slug => params[:slug]).first
    @pages = Page.all
    @titletext = @page.title + " (Title Area) - Edit Page"
    @metadescription = "Page editor with many parameters"
  end

  def create
     @titletext = "Create new page - Test Blog"
    @metadescription = "Create new page with many parameters"
    @pages = Page.all
    @page = Page.new(page_params)
      if @page.save
         redirect_to @page
      else 
    render 'new'
  end
  end
  
 def update
  @pages = Page.all
    @page = Page.where(:slug => params[:slug]).first
      @titletext = @page.title + " (Title Area) - Edit Page"
    @metadescription = "Page editor with many parameters"
      if @page.update(page_params)
         redirect_to @page
      else
      render 'edit'
    end
end

  def destroy
    @page = Page.where(:slug => params[:slug]).first
    @page.destroy
    redirect_to pages_path
  end

  def show
    @pages = Page.all
    @page = Page.where(:slug => params[:slug]).first
    @titletext = @page.title + " (Title Area) - READ PAGE MODE"
    @metadescription = @page.description
    @choosedPage = @page.slug
  end
     
  private
  def page_params
    params.require(:page).permit(:title, :description, :slug,:published, :menu_label, :h1, :content, :published_at, :priority)
  end

end 

