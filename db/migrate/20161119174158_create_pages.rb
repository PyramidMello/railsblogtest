class CreatePages < ActiveRecord::Migration[5.0]
  def change
    create_table :pages do |t|
      t.string :slug
      t.string :menu_label
      t.datetime :published_at
      t.boolean :published
      t.integer :priority
      t.string :title
      t.text :description
      t.text :h1
      t.text :content

      t.timestamps
    end
  end
end
