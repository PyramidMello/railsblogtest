Rails.application.routes.draw do
 resources :pages, param: :slug
 get 'pages/:slug' => 'pages#show'
 root :to => 'pages#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
